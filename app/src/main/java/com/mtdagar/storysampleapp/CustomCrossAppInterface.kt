package com.mtdagar.storysampleapp

import android.content.Intent
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.builders.ApiParamsBuilder
import com.touchtalent.bobblesdk.core.interfaces.CrossAppInterface
import com.touchtalent.bobblesdk.core.pojo.UserCredentials
import okhttp3.OkHttpClient

class CustomCrossAppInterface : CrossAppInterface{

    override fun baseUrl(): String = "https://api.bobbleapp.asia/v4"

    override fun baseUrlWithoutVersion(): String = "https://api.bobbleapp.asia/v4"

    override fun bobblificationUrl(): String = "https://bobblification.bobbleapp.me/v4"

    override fun getLoginCredentials(): UserCredentials? {
        return null
    }

    override fun handleGeneralANErrorResponse(anError: ANError, reference: String) {
        TODO("Not yet implemented")
    }

    private val _gson = Gson()

    override fun getGson(): Gson {
        return _gson
    }

    override fun getAppInviteLink(): String {
        return ""
    }

    override fun getActiveLanguageLocale(): String {
        return "en-IN"
    }

    override fun canLogEvents(): Boolean = false

    override fun logException(tag: String, exception: Throwable) {
        TODO("Not yet implemented")
    }

    override fun getVersion(): Int {
        return 99999999
    }

    override fun performVibration() {

    }

    override fun getBasicFont(otfText: String): String {
        return otfText
    }

    override fun sendOpenKeyboardIntent(sourceIntent: Intent?) {
        TODO("Not yet implemented")
    }

    override fun modifyActivityIntentForKeyboard(intent: Intent, deepLink: Int?) {
        TODO("Not yet implemented")
    }

    override fun getAdvertisingId(): String {
        return "dummy-advertising-id"
    }

    override fun isLimitAdTrackingEnabled(): Boolean {
        return false
    }

    override fun getCurrentPackageName(isKeyboardView: Boolean): String {
        return "com.mtdagar"
    }

    override fun getSessionId(isKeyboardView: Boolean): String {
        return "dummy-session-id"
    }

    override fun getOkHttpClient(): OkHttpClient {
        return BobbleCoreSDK.okHttpClient
    }

    override fun logEvents(
        eventType: String?,
        eventAction: String?,
        screenAt: String?,
        data: String?,
        logMultiple: Boolean
    ) {
        TODO("Not yet implemented")
    }

    override fun runInBootAwareMode(runnable: Runnable) {
        runnable.run()
    }

    override fun getAppName(): String {
        return "Story Sample App"
    }

    override fun getApiParamsBuilder(): ApiParamsBuilder {
        return object: ApiParamsBuilder() {
            override fun populateDeviceIdParams(hashMap: HashMap<String, String>, key: String?) {
            }

            override fun populateClientIdParams(hashMap: HashMap<String, String>, key: String?) {
            }

            override fun populateVersionParams(hashMap: HashMap<String, String>) {
            }

            override fun populateGeoLocationParams(hashMap: HashMap<String, String>) {
            }

            override fun populateUtmCampaignParams(hashMap: HashMap<String, String>) {
            }

            override fun populateDeviceManufacturer(hashMap: HashMap<String, String>) {
            }

            override fun populateAdvertisementId(hashMap: HashMap<String, String>) {
            }

            override fun populateLocationV2Params(hashMap: HashMap<String, String>) {
            }

            override fun populateGPSCoordinates(hashMap: HashMap<String, String>) {
            }

            override fun populateLimitAdTracking(hashMap: HashMap<String, String>) {
            }
        }
    }
}