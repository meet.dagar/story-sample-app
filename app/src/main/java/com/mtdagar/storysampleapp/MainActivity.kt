package com.mtdagar.storysampleapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mtdagar.storysampleapp.databinding.ActivityMainBinding
import com.touchtalent.bobblesdk.stories.data.StoriesRepository
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val storiesRepository = StoriesRepository()


        runBlocking {
            storiesRepository.getStories()
        }
    }


}