package com.mtdagar.storysampleapp

import android.app.Application
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig

class SampleApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        initBobbleCore()
    }

    private fun initBobbleCore() {
        val config = BobbleCoreConfig(CustomCrossAppInterface())

        BobbleCoreSDK.initialise(this, config)
    }

}